FROM node:16

WORKDIR /exemple

COPY package.json .

RUN yarn install

COPY . /exemple